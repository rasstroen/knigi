<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\KernelEvents;

require_once __DIR__ . '/../vendor/autoload.php';

header('X-Accel-Expires: 0');
session_cache_limiter('public');
error_reporting(E_ALL);
ini_set('display_errors', 'On');
setlocale(LC_TIME, "ru_RU.UTF-8");
date_default_timezone_set('Etc/GMT-3');

\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader('class_exists');
\Web\DI::init(require '../config/main.php');

$request = Request::createFromGlobals();

$context = new RequestContext();
$context->fromRequest($request);
$matcher = new UrlMatcher(\Web\Router::getRoutes(), $context);
$argumentResolver = new ArgumentResolver();
$eventDispatcher = new EventDispatcher();
$eventDispatcher->addListener(
    KernelEvents::EXCEPTION,
    [new \Web\Listeners\ExceptionConverterListener(), 'convert']
);
$eventDispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));

$kernel = new HttpKernel($eventDispatcher, new ControllerResolver(), new RequestStack(), $argumentResolver);
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
