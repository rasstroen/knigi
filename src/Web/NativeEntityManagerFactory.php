<?php

namespace Web;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

/**
 * Factory for creating native doctrine entity manager
 */
class NativeEntityManagerFactory
{
    /**
     * Config
     *
     * @var array
     */
    private $config;

    /**
     * NativeEntityManagerFactory constructor.
     *
     * @param array $config Database settings
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Create entity manager
     *
     * @return EntityManager
     * @throws \InvalidArgumentException
     * @throws \Doctrine\ORM\ORMException
     */
    public function create()
    {
        $proxyDir = __DIR__ . '/../../tmp';

        $autoGenerateProxies = $this->config['auto_generate_proxies'] ?? true;

        $entitiesPath = [__DIR__ . '/Entities'];
        $driver = new AnnotationDriver(new AnnotationReader(), $entitiesPath);

        $cache = new ArrayCache();

        $config = new Configuration();
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);
        $config->setResultCacheImpl($cache);
        $config->setProxyDir($proxyDir);
        $config->setProxyNamespace('DoctrineProxies');
        $config->setAutoGenerateProxyClasses($autoGenerateProxies);
        $config->setMetadataDriverImpl($driver);

        return EntityManager::create($this->config, $config);
    }
}
