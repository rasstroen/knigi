<?php

namespace Web\Services;

use Web\Entities\Genre;
use Web\Repository\GenreRepository;

class GenreService
{
    /**
     * @var GenreRepository
     */
    private $repository;

    /**
     * GenreService constructor.
     * @param GenreRepository $repository
     */
    public function __construct(GenreRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $name
     * @return Genre
     */
    public function createIfNotExists(string $name): Genre
    {
        $genre = $this->repository->findOneBy(
            [
                'name' => $name
            ]
        );

        if (!$genre) {
            $genre = new Genre($name);
            $this->repository->save($genre);
        }
        return $genre;
    }
}
