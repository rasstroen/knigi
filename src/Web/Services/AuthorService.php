<?php

namespace Web\Services;

use Web\Entities\Author;
use Web\Repository\AuthorRepository;

class AuthorService
{
    /**
     * @var AuthorRepository
     */
    private $repository;

    /**
     * AuthorService constructor.
     * @param AuthorRepository $repository
     */
    public function __construct(AuthorRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $firstName
     * @param string $middleName
     * @param string $lastName
     *
     * @return Author
     */
    public function createIfNotExists(string $firstName, string $middleName, string $lastName): Author
    {
        $author = $this->repository->findOneBy(
            [
                'lastName' => $lastName,
                'firstName' => $firstName,
                'middleName' => $middleName,
            ]
        );

        if (!$author) {
            $author = new Author($firstName, $middleName, $lastName);
            $this->repository->save($author);
        }
        return $author;
    }
}
