<?php

namespace Web\Services\Parser;

class Fb2Parser
{
    public function getHTML(string $filePath, string $htmlPath)
    {
        if (file_exists($htmlPath)) {
            return file_get_contents($htmlPath);
        }

        $xslTemplate = new \DOMDocument();
        $fb2Document = new \DOMDocument();
        $fb2Document->load($filePath);

        $xslProcessor = new \XSLTProcessor();
        $xslTemplate->load('../books/XSL/book.xsl', LIBXML_NOENT | LIBXML_DTDLOAD);
        $xslProcessor->importStyleSheet($xslTemplate);
        $htmlContent = $xslProcessor->transformToXML($fb2Document);

        return $htmlContent;
    }

    public function parse(string $filePath)
    {
        $metaData = [];
        $simpleXml = simplexml_load_string(str_replace('l:href', 'l-href', file_get_contents($filePath)));


        $description = $simpleXml->description ?? null;
        $lastName = 'last-name';
        $firstName = 'first-name';
        $middleName = 'middle-name';
        $homePage = 'home-page';

        if (!empty($description->{"publish-info"})) {
            foreach ($description->{"publish-info"}->children() as $key => $children) {
                switch ($key) {
                    case 'publisher':
                        $metaData['publish-info'][$key] = (string)$children;
                        break;
                    case 'city':
                        $metaData['publish-info'][$key] = (string)$children;
                        break;
                    case 'year':
                        $metaData['publish-info'][$key] = (string)$children;
                        break;
                    default:
                        $metaData['publish-info'][$key] = (string)$children;
                        break;
                }
            }
        }


        if (!empty($description->{"title-info"})) {
            foreach ($description->{"title-info"}->children() as $key => $children) {
                switch ($key) {
                    case 'author':
                    case 'translator':
                        $metaData[$key][] = [
                            $firstName => (string)$children->$firstName,
                            $lastName => (string)$children->$lastName,
                            $middleName => (string)$children->$middleName,
                            $homePage => (string)$children->$homePage,
                        ];
                        break;
                    case 'book-title':
                    case 'genre':
                        $metaData[$key][] = (string)$children;
                        break;
                    case 'coverpage':
                        $metaData['coverpage'] = (string)$children->image->attributes()->{"l-href"};
                        foreach ($simpleXml->binary as $children) {
                            $metaData['binary'][(string)$children->attributes()->{"content-type"}][(string)$children->attributes()->id] = (string)$children;
                        }
                        break;
                    case 'annotation':
                        foreach ($children->children() as $tagKey => $tag) {
                            $metaData['annotation'][] = '<' . $tagKey . '>' . (string)$tag . '</' . $tagKey . '>';
                        }
                        break;
                    default:
                        $metaData['description'][$key] = (string)$children;
                        break;
                }
            }
        }
        return new MetaData($metaData);
    }
}
