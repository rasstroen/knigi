<?php namespace Web\Services\Parser;

class MetaData
{
    /**
     * @var array
     */
    private $metaData;

    public function __construct(array $metaData)
    {
        $this->metaData = $metaData;
    }

    /**
     * @param string $imageType
     * @return null
     */
    public function getCoverImage(string $imageType)
    {
        if (!empty($this->metaData['binary'])) {
            return $this->metaData['binary'][$imageType][str_replace('#', '', $this->metaData['coverpage'])];
        }
        return null;
    }

    /**
     * @return array
     */
    public function getAuthors()
    {
        $authors = [];
        if (!empty($this->metaData['author'])) {
            foreach ($this->metaData['author'] as $author) {
                $authors[] =
                    [
                        'first_name' => $author['first-name'],
                        'last_name' => $author['last-name'],
                        'middle_name' => $author['middle-name'],
                    ];
            }
        }
        return $authors;
    }

    /**
     * @return array
     */
    public function getTranslators()
    {
        $translators = [];
        if (!empty($this->metaData['translator'])) {
            foreach ($this->metaData['translator'] as $translator) {
                $translators[] =
                    [
                        'first_name' => $translator['first-name'],
                        'last_name' => $translator['last-name'],
                        'middle_name' => $translator['middle-name'],
                    ];
            }
        }
        return $translators;
    }

    public function getTitle()
    {
        return implode(" ", $this->metaData['book-title']);
    }

    public function getLanguageId()
    {
        return $this->metaData['description']['lang'] ?? 'ru';
    }

    public function getSourceLanguageId()
    {
        return $this->metaData['description']['src-lang'] ?? 'ru';
    }

    public function getAnnotation()
    {
        return !empty($this->metaData['annotation']) ? implode("", $this->metaData['annotation']) : '';
    }

    public function getIsbn()
    {
        $isbn = !empty($this->metaData['publish-info']['isbn']) ? $this->metaData['publish-info']['isbn'] : '';

        $isbn = explode(',', $isbn);
        $isbn = trim($isbn[0]);
        $isbn = trim(preg_replace('/[^Х^X^x^\d^\-]/isU', '', $isbn));
        return $isbn;
    }

    /**
     * @return array
     */
    public function getGenres(): array
    {
        return $this->metaData['genre'] ?? [];
    }

    /**
     * @return bool
     */
    public function isBook()
    {
        return trim($this->getTitle()) !== '' ?? false;
    }
}
