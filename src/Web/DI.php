<?php

namespace Web;

use Web\Doctrine\DelayedEntityManagerDecorator;
use Web\Doctrine\RecoverableEntityManagerDecorator;
use Web\Entities\Author;
use Web\Entities\Genre;
use Web\Repository\AuthorRepository;
use Web\Repository\GenreRepository;
use Web\Services\AuthorService;
use Web\Services\GenreService;

class DI
{
    /**
     * @var DI
     */
    private static $instance;

    /**
     * @var array
     */
    private $configuration;

    public static function getInstance(): DI
    {
        return self::$instance;
    }

    public static function init(array $configuration)
    {
        self::$instance = new self($configuration);
    }

    /**
     * @return DelayedEntityManagerDecorator
     */
    protected function initializeDoctrine(): DelayedEntityManagerDecorator
    {
        $factory = new NativeEntityManagerFactory($this->configuration['database']);

        return new DelayedEntityManagerDecorator(new RecoverableEntityManagerDecorator($factory));
    }

    public function __construct(array $configuration)
    {
        $this->configuration = $configuration;
        $this->di['entityManager'] = $this->initializeDoctrine();

        $this->di['authorRepository'] = $this->getEntityManager()->getRepository(Author::class);
        $this->di['authorService'] = new AuthorService($this->getAuthorRepository());

        $this->di['genreRepository'] = $this->getEntityManager()->getRepository(Genre::class);
        $this->di['genreService'] = new GenreService($this->getGenreRepository());
    }

    /**
     * @return DelayedEntityManagerDecorator
     */
    public function getEntityManager()
    {
        return $this->di['entityManager'];
    }

    /**
     * @return AuthorRepository
     */
    public function getAuthorRepository()
    {
        return $this->di['authorRepository'];
    }

    /**
     * @return AuthorService
     */
    public function getAuthorService()
    {
        return $this->di['authorService'];
    }

    /**
     * @return GenreRepository
     */
    public function getGenreRepository()
    {
        return $this->di['genreRepository'];
    }

    /**
     * @return GenreService
     */
    public function getGenreService()
    {
        return $this->di['genreService'];
    }
}
