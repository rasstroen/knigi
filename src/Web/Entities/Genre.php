<?php

namespace Web\Entities;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Web\Repository\GenreRepository")
 * @ORM\Table(name="genre")
 */
class Genre
{
    /**
     * Id
     *
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * Genre name
     *
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 1,
     *      max = 52,
     * )
     */
    public $name;

    /**
     * Genre title
     *
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 1,
     *      max = 52,
     * )
     */
    public $title;

    /**
     * Genre constructor.
     *
     * @param null|string $name
     * @param null|string $title
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }
}
