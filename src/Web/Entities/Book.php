<?php

namespace Web\Entities;

class Book
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $publisherId;

    /**
     * @var int
     */
    private $year;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $annotation;

    /**
     * @var string
     */
    private $keywords;

    /**
     * @var string
     */
    private $lang;

    /**
     * @var string
     */
    private $sourceLang;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $hasCover;

    /**
     * Book constructor.
     *
     * @param int $publisherId
     * @param int $year
     * @param string $title
     * @param string $annotation
     * @param string $keywords
     * @param string $lang
     * @param string $sourceLang
     * @param string $description
     * @param string $hasCover
     */
    public function __construct(
        int $publisherId,
        int $year,
        string $title,
        string $annotation,
        string $keywords,
        string $lang,
        string $sourceLang,
        string $description,
        string $hasCover
    ) {
        $this->id = $id;
        $this->publisherId = $publisherId;
        $this->year = $year;
        $this->title = $title;
        $this->annotation = $annotation;
        $this->keywords = $keywords;
        $this->lang = $lang;
        $this->sourceLang = $sourceLang;
        $this->description = $description;
        $this->hasCover = $hasCover;
    }
}
