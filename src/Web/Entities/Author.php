<?php

namespace Web\Entities;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Web\Repository\AuthorRepository")
 * @ORM\Table(name="author")
 */
class Author
{
    /**
     * Id
     *
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * First name
     *
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 1,
     *      max = 52,
     * )
     */
    public $firstName;

    /**
     * Middle name
     *
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 1,
     *      max = 52,
     * )
     */
    public $middleName;

    /**
     * Last name
     *
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 1,
     *      max = 52,
     * )
     */
    public $lastName;

    /**
     * Author constructor.
     *
     * @param int $firstName
     * @param string $middleName
     * @param string $lastName
     */
    public function __construct(string $firstName, string $middleName, string $lastName)
    {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }
}
