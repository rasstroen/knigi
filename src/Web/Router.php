<?php

namespace Web;

use Web\Controllers\Admin\BooksController;
use Web\Controllers\IndexController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Router
{
    /**
     * Get all routes
     *
     * @return RouteCollection All routes
     */
    public static function getRoutes(): RouteCollection
    {
        $routeDefinitions = self::getRouteDefinitions();
        $rootRoutes = new RouteCollection();

        foreach ($routeDefinitions as $routeDefinition) {
            $route = new Route(
                $routeDefinition['path'],
                // http://symfony.com/doc/current/routing.html#special-routing-parameters
                ['_controller' => $routeDefinition['controller']],
                $routeDefinition['requirements'] ?? [], // requirements
                [], // options
                '', // host
                ['http', 'https'], // schemas
                [$routeDefinition['httpMethod']]
            );
            $rootRoutes->add($routeDefinition['name'], $route);
        }

        return $rootRoutes;
    }

    /**
     * Get route definitions
     *
     * @return string[] Route definitions
     */
    protected static function getRouteDefinitions(): array
    {
        $routeDefinitions = [
            // index
            [
                'name' => 'index',
                'path' => '',
                'controller' => IndexController::class . '::indexAction',
                'httpMethod' => 'GET',
            ],
            // admin books list
            [
                'name' => 'admin/booksList',
                'path' => '/admin/books',
                'controller' => BooksController::class . '::listAction',
                'httpMethod' => 'GET',
            ],
            // admin adding book
            [
                'name' => 'admin/bookAddForm',
                'path' => '/admin/books/add',
                'controller' => BooksController::class . '::addFormAction',
                'httpMethod' => 'GET',
            ],
            // admin adding book
            [
                'name' => 'admin/bookAdd',
                'path' => '/admin/books/add',
                'controller' => BooksController::class . '::addAction',
                'httpMethod' => 'POST',
            ],
        ];

        return $routeDefinitions;
    }
}
