<?php

namespace Web\Doctrine;

/**
 * Interface EntityManagerInterface
 */
interface EntityManagerInterface extends \Doctrine\ORM\EntityManagerInterface
{
    /**
     * Refresh object and return it
     *
     * @see EntityManagerInterface::refresh()
     * @param mixed $object Object
     * @return mixed
     */
    public function refreshAndReturn($object);
}
