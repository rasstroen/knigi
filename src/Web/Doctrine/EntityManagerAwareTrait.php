<?php

namespace Web\Doctrine;

use Web\DI;

/**
 * EntityManagerAwareTrait
 */
trait EntityManagerAwareTrait
{
    /**
     * EntityManager
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Get EntityManager
     *
     * @return EntityManagerInterface
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        if (!$this->entityManager || !$this->entityManager->isOpen()) {
            $this->entityManager = DI::getInstance()->getEntityManager();
        }

        return $this->entityManager;
    }

    /**
     * Set EntityManager
     *
     * @param EntityManagerInterface $entityManager EntityManager
     */
    protected function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}
