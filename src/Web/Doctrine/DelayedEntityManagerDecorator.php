<?php

namespace Web\Doctrine;

use Doctrine\ORM\Decorator\EntityManagerDecorator;

/**
 * This decorator can delay flush requests
 *
 * @property EntityManagerInterface
 */
class DelayedEntityManagerDecorator extends EntityManagerDecorator implements EntityManagerInterface
{
    /**
     * Should we flush
     *
     * @var boolean
     */
    private $shouldFlush = true;

    /**
     * DelayedEntityManagerDecorator constructor.
     *
     * @param EntityManagerInterface $wrapped Entity manager
     */
    public function __construct(EntityManagerInterface $wrapped)
    {
        parent::__construct($wrapped);
        $this->wrapped = $wrapped;
    }

    /**
     * Do not flush next requests
     */
    public function doNotFlush()
    {
        $this->shouldFlush = false;
    }

    /**
     * Do flush next requests
     */
    public function doFlush()
    {
        $this->shouldFlush = true;
    }

    /**
     * Force flush
     *
     * @param null $entity
     */
    public function forceFlush($entity = null)
    {
        parent::flush($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function flush($entity = null)
    {
        if ($this->shouldFlush) {
            parent::flush($entity);
        }
    }

    /**
     * {@inheritdoc}
     * @throws \RuntimeException
     */
    public function refreshAndReturn($object)
    {
        return $this->wrapped->refreshAndReturn($object);
    }
}
