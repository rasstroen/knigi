<?php

namespace Web\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\Decorator\EntityManagerDecorator as BaseEntityManagerDecorator;
use Web\NativeEntityManagerFactory;

/**
 * RecoverableEntityManagerDecorator
 *
 * This entity manager can manage UniqueConstraintViolationException.
 * If this exception is thrown this decorator create a new entity manager.
 * When manager tries to refresh entity it persist before automatically if the entity isn't managed by manager.
 */
class RecoverableEntityManagerDecorator extends BaseEntityManagerDecorator implements EntityManagerInterface
{
    /**
     * Factory for recreating entity manager
     *
     * @var NativeEntityManagerFactory
     */
    private $factory;

    /**
     * RecoverableEntityManagerDecorator constructor.
     *
     * @param NativeEntityManagerFactory $factory Factory
     * @noinspection MagicMethodsValidityInspection
     * @throws \Doctrine\ORM\ORMException
     * @throws \InvalidArgumentException
     */
    public function __construct(NativeEntityManagerFactory $factory)
    {
        $em = $factory->create();
        $this->factory = $factory;
        parent::__construct($em);
    }

    /**
     * Catch UniqueConstraintViolationException and recreate entity manager
     *
     * {@inheritdoc}
     */
    public function flush($entity = null)
    {
        try {
            parent::flush($entity);
        } catch (UniqueConstraintViolationException $e) {
            $this->wrapped = $this->factory->create();
            throw $e;
        }
    }

    /**
     * Pass current decorator to all repositories,
     * because native entity manager pass itself for all repositories by default.
     *
     * {@inheritdoc}
     */
    public function getRepository($className)
    {
        $factory = $this->getConfiguration()->getRepositoryFactory();

        return $factory->getRepository($this, $className);
    }

    /**
     * {@inheritdoc}
     */
    public function close()
    {
        $result = parent::close();
        $this->wrapped->clear();
        $this->wrapped = $this->factory->create();
        $this->wrapped->getUnitOfWork()->clear();
        return $result;
    }

    /**
     * Refresh and return entity
     *
     * @see ObjectManager::refresh()
     * @param mixed $object Object
     * @return mixed
     */
    public function refreshAndReturn($object)
    {
        if (!$this->contains($object)) {
            $object = $this->merge($object);
        }
        parent::refresh($object);

        return $object;
    }
}
