<?php

namespace Web\Response;

class ModuleResponse
{
    /**
     * @var string
     */
    private $blockName;

    /**
     * @var string
     */
    private $moduleName;

    /**
     * @var string
     */
    private $template;

    /**
     * @var array
     */
    private $data;

    /**
     * ModuleResponse constructor.
     * @param string $blockName
     * @param string $moduleName
     * @param string $template
     * @param array $data
     */
    public function __construct(string $blockName, string $moduleName, string $template, array $data)
    {
        $this->blockName = $blockName;
        $this->moduleName = $moduleName;
        $this->template = $template;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getBlockName(): string
    {
        return $this->blockName;
    }

    /**
     * @return string
     */
    public function getModuleName(): string
    {
        return $this->moduleName;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
