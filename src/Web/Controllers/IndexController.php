<?php

namespace Web\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Web\Response\ModuleResponse;
use Web\View\View;

class IndexController extends AbstractController
{
    public function indexAction(Request $request): Response
    {
        $modules['logo'] = new ModuleResponse('header', 'logo', 'common/header.twig', []);

        return new Response((new View())->render('index', $modules));
    }
}
