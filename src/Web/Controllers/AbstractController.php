<?php

namespace Web\Controllers;

use Web\DI;

class AbstractController
{
    /**
     * @return \Web\DI
     */
    protected function getDi()
    {
        return DI::getInstance();
    }
}
