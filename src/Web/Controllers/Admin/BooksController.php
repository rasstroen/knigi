<?php

namespace Web\Controllers\Admin;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Web\Controllers\AbstractController;
use Web\Entities\Author;
use Web\Response\ModuleResponse;
use Web\Services\Parser\Fb2Parser;
use Web\View\View;

class BooksController extends AbstractController
{
    public function listAction(Request $request): Response
    {
        $modules['logo'] = new ModuleResponse('header', 'logo', 'common/header.twig', []);

        return new Response((new View())->render('admin', $modules));
    }

    public function addFormAction(Request $request): Response
    {
        $modules['logo'] = new ModuleResponse('header', 'logo', 'common/header.twig', []);
        $modules['form'] = new ModuleResponse('content', 'form', 'modules/admin/books/add.twig', []);

        return new Response((new View())->render('admin', $modules));
    }

    public function addAction(Request $request): Response
    {
        /**
         * @var UploadedFile $file
         */
        $file = $request->files->get('file');
        $parser = new Fb2Parser();
        $meta = $parser->parse($file->getRealPath());

        $authors = [];

        foreach ($meta->getAuthors() as $authorData) {
            $authors[] = $this->getDi()->getAuthorService()->createIfNotExists(
                $authorData['first_name'] ?? '',
                $authorData['middle_name'] ?? '',
                $authorData['last_name'] ?? ''
            );
        }

        foreach ($meta->getGenres() as $genre) {
            $genres[] = $this->getDi()->getGenreService()->createIfNotExists($genre);
        }

        return new Response();
    }
}
