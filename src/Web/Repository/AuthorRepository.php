<?php

namespace Web\Repository;

use Web\Repository\AbstractRepository;
use Web\Entities\Author;

class AuthorRepository extends AbstractRepository
{
    public function getEntityClass(): string
    {
        return Author::class;
    }
}
