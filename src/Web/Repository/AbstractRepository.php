<?php

namespace Web\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Custom entity repository
 *
 * @see http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/working-with-objects.html#custom-repositories
 */
abstract class AbstractRepository extends EntityRepository
{
    /**
     * Get entity class name
     *
     * @return string Entity class name
     */
    abstract protected function getEntityClass(): string;

    /**
     * Save entity
     *
     * @param mixed $entity Entity
     */
    public function save($entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
    }

    public function create()
    {
    }

    /**
     * @inheritdoc
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->_em;
    }
}
