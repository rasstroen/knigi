<?php

namespace Web\Repository;

use Web\Entities\Genre;

class GenreRepository extends AbstractRepository
{
    public function getEntityClass(): string
    {
        return Genre::class;
    }
}
