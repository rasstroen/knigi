<?php

namespace Web\Listeners;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Class ExceptionConverterListener
 */
class ExceptionConverterListener
{
    /**
     * Convert exception to response
     *
     * @param GetResponseForExceptionEvent $event Event
     */
    public function convert(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $event->setResponse(
            new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR)
        );
    }
}
