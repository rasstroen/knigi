<?php

declare(strict_types=1);

namespace Web\View\Exception;

class IllegalPathException extends \Exception
{
}
