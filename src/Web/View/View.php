<?php

declare(strict_types=1);

namespace Web\View;

use Web\Response\ModuleResponse;
use Web\View\Exception\IllegalPathException;

class View
{
    /**
     * @param $layout
     * @param ModuleResponse[] $moduleResponses
     *
     * @return false|string
     *
     * @throws IllegalPathException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function render(string $layout, array $moduleResponses)
    {
        foreach ($moduleResponses as $moduleResponse) {
            $data['modules'][$moduleResponse->getBlockName()][$moduleResponse->getModuleName()] = [
                'template' => $moduleResponse->getTemplate(),
                'data' => $moduleResponse->getData()
            ];
        }

        $projectTemplatesRoot = __DIR__ . '/../../../templates/';

        if (null === $projectTemplatesRoot) {
            throw new IllegalPathException('No path specified for project templates');
        }

        $loader = new \Twig_Loader_Filesystem(
            [
                $projectTemplatesRoot,
            ]
        );
        $params = [];

        $twig = new \Twig_Environment($loader, $params);

        $template = $twig->loadTemplate('internal/bootstrap.twig');

        ob_start();
        echo $template->render(
            [
                'layout' => 'layouts' . DIRECTORY_SEPARATOR . $layout . '.twig',
                'data' => $data
            ]
        );

        return ob_get_clean();
    }
}
